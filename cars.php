<?php
class cars{
    private $filearray = array('brand', 'maxSpeed', 'color', 'weight', 'numberOfSeats');

    function __construct($brand, $maxSpeed, $color, $weight, $numberOfSeats){
        if ($maxSpeed<0) {
            throw new Exception('Скорость не может быть негативной');
        }
        else {
            if ($maxSpeed>300){
                throw new Exception('Скорость не может быть больше 300 км/ч');
            }
            else {
                $this->filearray['brand'] = $brand;
                $this->filearray['maxSpeed'] = round($maxSpeed/3.6,2);
                $this->filearray['color'] = $color;
                $this->filearray['weight'] = $weight;
                $this->filearray['numberOfSeats'] = $numberOfSeats;
            }
        }
    }

   function __get($key){
        return $this->$key;
    }

    function __set($key,$value){
        $this->$key=$value;
    }
}

?>