<html>
<head>
    <title>Машины</title>
    <meta charset="UTF-8">
</head>
<body>
<?php
require 'cars.php';
try{
    $car1 = new cars("Hyundai Getz", 167, "серебристый", 920, 4);
    echo 'Марка: ' . $car1->filearray['brand'] . ', максимальная скорость: ' . $car1->filearray['maxSpeed'] . ' м/с' . ', цвет: '. $car1->filearray['color'] . ', вес: ' . $car1->filearray['weight'] . ' кг' . ', количество посадочных мест: ' . $car1->filearray['numberOfSeats'] . '<br>';
}

catch(Exception $ex)
{
    echo 'Ошибка: ' . $ex->getMessage() . '<br>';
}
try{
    $car2 = new cars("Mazda 323F", 204, "красный", 1210, 4);
    echo 'Марка: ' . $car2->filearray['brand'] . ', максимальная скорость: ' . $car2->filearray['maxSpeed'] . ' м/с' . ', цвет: '. $car2->filearray['color'] . ', вес: ' . $car2->filearray['weight'] . ' кг' . ', количество посадочных мест: ' . $car2->filearray['numberOfSeats'] . '<br>';
}
catch(Exception $ex)
{
    echo 'Ошибка: ' . $ex->getMessage() . '<br>';
}
?>
</body>
</html>